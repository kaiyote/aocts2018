import { Expect, SetupFixture, Test, TestFixture } from 'alsatian'
import Day7 from '../src/Day7'
import { ITestBase } from '../src/IDay'

@TestFixture('Day 7 Tests')
export default class Day7Test implements ITestBase {
  runner?: Day7

  @SetupFixture
  setupFixture () {
    this.runner = new Day7()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    let result = this.runner!.part1(`Step C must be finished before step A can begin.
    Step C must be finished before step F can begin.
    Step A must be finished before step B can begin.
    Step A must be finished before step D can begin.
    Step B must be finished before step E can begin.
    Step D must be finished before step E can begin.
    Step F must be finished before step E can begin.`)
    Expect(result).toBe('CABDFE')
    result = this.runner!.part1(this.data)
    Expect(result).toBe('AEMNPOJWISZCDFUKBXQTHVLGRY')
  }

  @Test('Part 2 behaves')
  testPart2 () {
    this.runner!.baseStepTime = 0
    this.runner!.numberOfWorkers = 2
    let result = this.runner!.part2(`Step C must be finished before step A can begin.
    Step C must be finished before step F can begin.
    Step A must be finished before step B can begin.
    Step A must be finished before step D can begin.
    Step B must be finished before step E can begin.
    Step D must be finished before step E can begin.
    Step F must be finished before step E can begin.`)
    Expect(result).toBe(15)

    this.runner!.baseStepTime = 60
    this.runner!.numberOfWorkers = 5
    result = this.runner!.part2(this.data)
    Expect(result).toBe(1081)
  }

  // tslint:disable-next-line:member-ordering max-line-length
  data: string = `Step A must be finished before step N can begin.
  Step P must be finished before step R can begin.
  Step O must be finished before step T can begin.
  Step J must be finished before step U can begin.
  Step M must be finished before step X can begin.
  Step E must be finished before step X can begin.
  Step N must be finished before step T can begin.
  Step W must be finished before step G can begin.
  Step Z must be finished before step D can begin.
  Step F must be finished before step Q can begin.
  Step U must be finished before step L can begin.
  Step I must be finished before step X can begin.
  Step X must be finished before step Y can begin.
  Step D must be finished before step Y can begin.
  Step S must be finished before step K can begin.
  Step C must be finished before step G can begin.
  Step K must be finished before step V can begin.
  Step B must be finished before step R can begin.
  Step Q must be finished before step L can begin.
  Step T must be finished before step H can begin.
  Step H must be finished before step G can begin.
  Step V must be finished before step L can begin.
  Step L must be finished before step R can begin.
  Step G must be finished before step Y can begin.
  Step R must be finished before step Y can begin.
  Step G must be finished before step R can begin.
  Step X must be finished before step V can begin.
  Step V must be finished before step Y can begin.
  Step Z must be finished before step U can begin.
  Step U must be finished before step R can begin.
  Step J must be finished before step Y can begin.
  Step Z must be finished before step C can begin.
  Step O must be finished before step L can begin.
  Step C must be finished before step H can begin.
  Step V must be finished before step G can begin.
  Step F must be finished before step K can begin.
  Step Q must be finished before step G can begin.
  Step S must be finished before step Q can begin.
  Step M must be finished before step G can begin.
  Step T must be finished before step L can begin.
  Step C must be finished before step Q can begin.
  Step T must be finished before step V can begin.
  Step W must be finished before step Z can begin.
  Step C must be finished before step K can begin.
  Step I must be finished before step C can begin.
  Step X must be finished before step Q can begin.
  Step F must be finished before step X can begin.
  Step J must be finished before step S can begin.
  Step I must be finished before step K can begin.
  Step U must be finished before step Q can begin.
  Step I must be finished before step Q can begin.
  Step N must be finished before step H can begin.
  Step A must be finished before step T can begin.
  Step T must be finished before step G can begin.
  Step D must be finished before step T can begin.
  Step A must be finished before step X can begin.
  Step D must be finished before step G can begin.
  Step C must be finished before step T can begin.
  Step W must be finished before step Q can begin.
  Step W must be finished before step K can begin.
  Step V must be finished before step R can begin.
  Step H must be finished before step R can begin.
  Step F must be finished before step H can begin.
  Step F must be finished before step V can begin.
  Step U must be finished before step T can begin.
  Step K must be finished before step H can begin.
  Step B must be finished before step T can begin.
  Step H must be finished before step Y can begin.
  Step J must be finished before step Z can begin.
  Step B must be finished before step Y can begin.
  Step I must be finished before step V can begin.
  Step W must be finished before step V can begin.
  Step Q must be finished before step R can begin.
  Step I must be finished before step S can begin.
  Step E must be finished before step H can begin.
  Step J must be finished before step B can begin.
  Step S must be finished before step G can begin.
  Step E must be finished before step S can begin.
  Step N must be finished before step I can begin.
  Step Z must be finished before step F can begin.
  Step E must be finished before step I can begin.
  Step S must be finished before step B can begin.
  Step D must be finished before step L can begin.
  Step Q must be finished before step T can begin.
  Step Q must be finished before step H can begin.
  Step K must be finished before step Y can begin.
  Step M must be finished before step U can begin.
  Step U must be finished before step K can begin.
  Step W must be finished before step I can begin.
  Step J must be finished before step W can begin.
  Step K must be finished before step T can begin.
  Step P must be finished before step Y can begin.
  Step L must be finished before step G can begin.
  Step K must be finished before step B can begin.
  Step I must be finished before step Y can begin.
  Step U must be finished before step B can begin.
  Step P must be finished before step O can begin.
  Step O must be finished before step W can begin.
  Step O must be finished before step J can begin.
  Step A must be finished before step J can begin.
  Step F must be finished before step G can begin.`
}
