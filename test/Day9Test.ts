import { Expect, IgnoreTest, SetupFixture, Test, TestFixture } from 'alsatian'
import Day9 from '../src/Day9'
import { ITestBase } from '../src/IDay'

@TestFixture('Day 9 Tests')
export default class Day9Test implements ITestBase {
  runner?: Day9

  @SetupFixture
  setupFixture () {
    this.runner = new Day9()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    let result = this.runner!.part1('9 players; last marble is worth 25 points')
    Expect(result).toBe(32)
    result = this.runner!.part1('10 players; last marble is worth 1618 points')
    Expect(result).toBe(8317)
    result = this.runner!.part1('13 players; last marble is worth 7999 points')
    Expect(result).toBe(146373)
    result = this.runner!.part1('17 players; last marble is worth 1104 points')
    Expect(result).toBe(2764)
    result = this.runner!.part1('21 players; last marble is worth 6111 points')
    Expect(result).toBe(54718)
    result = this.runner!.part1('30 players; last marble is worth 5807 points')
    Expect(result).toBe(37305)
    result = this.runner!.part1(this.data)
    Expect(result).toBe(383475)
  }

  @IgnoreTest('slow af')
  @Test('Part 2 behaves')
  testPart2 () {
    const result = this.runner!.part2(this.data)
    Expect(result).toBe(3148209772)
  }

  // tslint:disable-next-line:member-ordering max-line-length
  data: string = '465 players; last marble is worth 71498 points'
}
