import { IDay } from './IDay'

export default class Day7 implements IDay<string | number> {
  numberOfWorkers?: number
  baseStepTime?: number
  private keys: string[] = '_ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

  part1 (input: string): string {
    return this.step(this.prepareInput(input))
  }

  part2 (input: string): number {
    const workers: Array<{step: string, timer: number}> = []
    for (let i = 0; i < this.numberOfWorkers!; i++) workers[i] = { step: '', timer: 0 }
    return this.step2(this.prepareInput(input), workers)
  }

  private prepareInput (input: string) {
    return input.split('\n')
      .map(x => /(.) must be finished before step (.)/.exec(x)!.splice(1, 2))
      .reduce((prev, curr) => {
        const before = curr[0]
        const after = curr[1]
        if (!prev[before]) prev[before] = new Set()
        if (!prev[after]) prev[after] = new Set()
        prev[after].add(before)
        return prev
      }, {} as {[index: string]: Set<string>})
  }

  private step (map: {[index: string]: Set<string>}, steps: string = ''): string {
    if (Object.keys(map).length === 0) return steps

    const next = Object.keys(map).filter(k => map[k].size === 0).sort()[0]
    delete map[next]
    Object.keys(map).forEach(k => map[k].delete(next))

    return this.step(map, steps + next)
  }

  private step2 (map: {[index: string]: Set<string>},
                 workerTimers: Array<{step: string, timer: number}>, totalTime: number = 0): number {
    if (Object.keys(map).length === 0) return totalTime + Math.max(...workerTimers.map(x => x.timer))

    for (const worker of workerTimers.filter(x => x.timer <= 0)) {
      const next = Object.keys(map).sort().find(k => map[k].size === 0)
      if (!next) break
      worker.step = next
      worker.timer = this.baseStepTime! + this.keys.findIndex(x => x === next)
      delete map[next]
    }

    // time skip
    const minTime = Math.min(...workerTimers.filter(x => x.timer > 0).map(x => x.timer))
    workerTimers.forEach(x => {
      x.timer -= minTime
      if (x.timer > 0) return
      Object.keys(map).forEach(y => map[y].delete(x.step))
      x.step = ''
    })

    return this.step2(map, workerTimers, totalTime + minTime)
  }
}
