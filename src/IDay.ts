export interface IDay<T = void> {
  part1: (data: string) => T
  part2: (data: string) => T
}

export interface ITestBase {
  data: string
  testPart1: () => void
  testPart2: () => void
}
