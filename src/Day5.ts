import { IDay } from './IDay'

export default class Day5 implements IDay<number> {
  part1 (input: string): number {
    const stream = this.prepareInput(input).split('')
    return this.react(stream)
  }

  part2 (input: string): number {
    const stream = this.prepareInput(input)

    const lens = 'abcdefghijklmnopqrstuvwxyz'.split('')
      .map(char => new RegExp(char, 'ig'))
      .map(reg => stream.replace(reg, ''))
      .map(str => this.react(str.split('')))

    return Math.min(...lens)
  }

  private prepareInput (input: string): string {
    return input.trim()
  }

  private react (polymer: string[]): number {
    const stack: string[] = []
    stack.push(polymer.splice(0, 1)[0])
    polymer.forEach(x => {
      const newIdx = stack.push(x) - 1
      const prev = stack[newIdx - 1]
      if (!!prev && x !== prev && (x === prev.toLowerCase() || x === prev.toUpperCase())) {
        stack.splice(newIdx - 1, 2)
      }
    })

    return stack.length
  }
}
