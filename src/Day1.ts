import { IDay } from './IDay'

export default class Day1 implements IDay<number> {
  part1 (input: string): number {
    return this.prepareInput(input).reduce((prev, curr) => prev + curr, 0)
  }

  part2 (input: string): number {
    const shifts = this.prepareInput(input)
    let freqs = new Set([0])
    let freq = 0
    while (true) {
      for (const shift of shifts) {
        freq += shift
        if (freqs.has(freq)) return freq
        else freqs = freqs.add(freq)
      }
    }
  }

  private prepareInput (input: string): number[] {
    return input.split('\n').map(s => +s.trim())
  }
}
