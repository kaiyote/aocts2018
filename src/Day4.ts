import { IDay } from './IDay'

export default class Day4 implements IDay<number> {
  part1 (input: string): number {
    const sortedWakeSleep = this.prepareInput(input)

    const guardMap: IGuardSleep = {}
    let sleepStart: number = 0
    for (const gs of sortedWakeSleep) {
      if (!guardMap[gs.guard]) guardMap[gs.guard] = new Array(60).fill(0)
      if (gs.sleep) {
        sleepStart = gs.timestamp.getMinutes()
      } else {
        const sleepEnd = gs.timestamp.getMinutes()
        guardMap[gs.guard] = guardMap[gs.guard].map((x, i) => i >= sleepStart && i < sleepEnd ? x + 1 : x)
      }
    }

    let maxSleepGuard: number = 0
    let maxSleepTime: number = 0
    Object.keys(guardMap).forEach(g => {
      const key = +g
      const sleepTime = guardMap[key].reduce((prev, curr) => prev + curr)
      if (sleepTime > maxSleepTime) {
        maxSleepTime = sleepTime
        maxSleepGuard = key
      }
    })

    const maxSleep = Math.max(...guardMap[maxSleepGuard])
    const maxSleepMinute = guardMap[maxSleepGuard].findIndex(x => x === maxSleep)

    return maxSleepGuard * maxSleepMinute
  }

  part2 (input: string): number {
    const sortedWakeSleep = this.prepareInput(input)

    const guardMap: IGuardSleep = {}
    let sleepStart: number = 0
    for (const gs of sortedWakeSleep) {
      if (!guardMap[gs.guard]) guardMap[gs.guard] = new Array(60).fill(0)
      if (gs.sleep) {
        sleepStart = gs.timestamp.getMinutes()
      } else {
        const sleepEnd = gs.timestamp.getMinutes()
        guardMap[gs.guard] = guardMap[gs.guard].map((x, i) => i >= sleepStart && i < sleepEnd ? x + 1 : x)
      }
    }

    let maxSleepGuard: number = 0
    let maxSleepTime: number = 0
    Object.keys(guardMap).forEach(g => {
      const key = +g
      const sleepTime = Math.max(...guardMap[key])
      if (sleepTime > maxSleepTime) {
        maxSleepTime = sleepTime
        maxSleepGuard = key
      }
    })

    const maxSleepMinute = guardMap[maxSleepGuard].findIndex(x => x === maxSleepTime)
    return maxSleepGuard * maxSleepMinute
  }

  private prepareInput (input: string): IGuardTime[] {
    const parsedArr: IGuardTime[] = []
    let currentGuard: number = 0
    const unparsed = input.split('\n')
      .map(x => x.trim().substring(1).split(']').map(y => y.trim()))
      .map(x => ({
        timestamp: new Date(x[0]),
        description: x[1]
      }))
      .sort((a, b) => a.timestamp.valueOf() - b.timestamp.valueOf())

    unparsed.forEach(x => {
      if (x.description.match(/Guard #(\d+)/)) {
        currentGuard = +x.description.match(/Guard #(\d+)/)![1]
      } else {
        parsedArr.push({
          timestamp: x.timestamp,
          guard: currentGuard,
          sleep: !!x.description.match(/falls asleep/)
        })
      }
    })

    return parsedArr
  }
}

interface IGuardTime {
  timestamp: Date
  guard: number
  sleep: boolean
}

interface IGuardSleep {
  [index: number]: number[]
}
