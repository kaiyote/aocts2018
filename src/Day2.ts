import { IDay } from './IDay'

export default class Day2 implements IDay<number | string | void> {
  part1 (input: string): number {
    const ids = this.prepareInput(input).map(x => x.sort())
    const letterCount = ids.map(id => id.reduce((counter, c) => {
      counter[c] = (counter[c] || 0) + 1
      return counter
    }, {} as {[index: string]: number}))
    const twoCount = letterCount.filter(x => {
      for (const key in x) if (x[key] === 2) return true
      return false
    }).length

    const threeCount = letterCount.filter(x => {
      for (const key in x) if (x[key] === 3) return true
      return false
    }).length

    return twoCount * threeCount
  }

  part2 (input: string): string | void {
    const ids = this.prepareInput(input).map(x => x.join(''))

    for (const id of ids) {
      if (ids.find(id2 => this.levenstein(id, id2) === 1)) {
        const id2 = ids.find(id3 => this.levenstein(id, id3) === 1)
        const output = []
        for (let i = 0; i < id.length; i++) {
          if (id[i] === id2![i]) output.push(id[i])
        }
        return output.join('')
      }
    }
  }

  private prepareInput (input: string): string[][] {
    return input.split('\n').map(x => x.trim().split(''))
  }

  private levenstein (a: string, b: string) {
    const m = []
    const min = Math.min

    for (let i = 0; i <= b.length; m[i] = [i++]) { /* do nothing */ }
    for (let j = 0; j <= a.length; m[0][j] = j++) { /* do nothing */ }

    for (let i = 1; i <= b.length; i++) {
      for (let j = 1; j <= a.length; j++) {
        m[i][j] = b.charAt(i - 1) === a.charAt(j - 1)
            ? m[i - 1][j - 1]
            : m[i][j] = min(
                m[i - 1][j - 1] + 1,
                min(m[i][j - 1] + 1, m[i - 1 ][j] + 1))
      }
    }

    return m[b.length][a.length]
  }
}
