import { IDay } from './IDay'

export default class Day3 implements IDay<number> {
  part1 (input: string): number {
    const squares = this.prepareInput(input)
    const map: number[][][] = []
    for (const square of squares) {
      for (let i = square.startPoint.x; i < square.startPoint.x + square.size.width; i++) {
        for (let j = square.startPoint.y; j < square.startPoint.y + square.size.height; j++) {
          if (!map[i]) map[i] = []
          if (!map[i][j]) map[i][j] = []
          map[i][j].push(square.elfNumber)
        }
      }
    }

    return map.reduce((prev, curr) => prev + curr.filter(x => x.length > 1).length, 0)
  }

  part2 (input: string): number {
    const squares = this.prepareInput(input)
    const map: number[][][] = []
    const shareElfs: Set<number> = new Set()
    for (const square of squares) {
      for (let i = square.startPoint.x; i < square.startPoint.x + square.size.width; i++) {
        for (let j = square.startPoint.y; j < square.startPoint.y + square.size.height; j++) {
          if (!map[i]) map[i] = []
          if (!map[i][j]) map[i][j] = []
          map[i][j].push(square.elfNumber)
        }
      }
    }

    map.flatMap(x => x.filter(y => y.length > 1).forEach(y => y.forEach(z => shareElfs.add(z))))

    return squares.map(x => x.elfNumber).find(x => !shareElfs.has(x))!
  }

  private prepareInput (input: string): IElfSquare[] {
    return input.split('\n').map(x => x.trim().split(' ')).map(x => ({
      elfNumber: +x[0].substring(1),
      startPoint: x[2].split(',').reduce((prev, curr, i) => {
        if (i === 0) prev.x = +curr
        else prev.y = +curr.split(':')[0]
        return prev
      }, { x: 0, y: 0 }),
      size: x[3].split('x').reduce((prev, curr, i) => {
        if (i === 0) prev.width = +curr
        else prev.height = +curr
        return prev
      }, { width: 0, height: 0 })
    }))
  }
}

interface IElfSquare {
  elfNumber: number
  startPoint: { x: number, y: number }
  size: { width: number, height: number }
}
