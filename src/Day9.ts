import { IDay } from './IDay'

export default class Day9 implements IDay<number> {
  part1 (input: string): number {
    const [players, lastMarble] = this.prepareInput(input)
    return this.process(players, lastMarble)
  }

  part2 (input: string): number {
    const [players, lastMarble] = this.prepareInput(input)
    return this.process(players, lastMarble * 100)
  }

  private prepareInput (input: string): number[] {
    return /(\d+).+?(\d+)/.exec(input)!.splice(1, 2).map(x => +x)
  }

  private process (players: number, lastMarble: number) {
    const marbles = [0, 2, 1]
    const score = new Array(players)
    score.fill(0)
    let currentMarble = 1

    // tslint:disable-next-line:one-variable-per-declaration
    for (let i = 3, cp = 2; i <= lastMarble; i++, cp === players - 1 ? cp = 0 : cp++) {
      if (i % 23 === 0) {
        const nextCurrent = currentMarble - 7
        const extra = marbles.splice(nextCurrent, 1)[0]
        score[cp] += (i + extra)
        currentMarble = nextCurrent
        if (currentMarble < 0) currentMarble = marbles.length + currentMarble + 1
      } else {
        let indexToSplice = currentMarble + 2
        if (indexToSplice > marbles.length) indexToSplice -= marbles.length
        marbles.splice(indexToSplice, 0, i)
        currentMarble = indexToSplice
      }
    }

    return Math.max(...score)
  }
}
