import { IDay } from './IDay'

export default class Day6 implements IDay<number> {
  searchDist?: number
  part1 (input: string): number {
    const coords = this.prepareInput(input)
    const maxX = Math.max(...coords.map(x => x.x)) + 1
    const maxY = Math.max(...coords.map(x => x.y)) + 1

    const field: number[][] = []
    for (let i = 0; i < maxX; i++) {
      field[i] = []
      for (let j = 0; j < maxY; j++) {
        field[i][j] = -1
      }
    }

    coords.forEach(x => field[x.x - 1][x.y - 1] = x.index)
    this.fill(field, coords)

    const infiniteIndexes = new Set()
    for (let i = 0; i < maxX; i += maxX - 1) {
      for (let j = 0; j < maxY; j++) {
        infiniteIndexes.add(field[i][j])
      }
    }
    for (let i = 0; i < maxX; i++) {
      for (let j = 0; j < maxY; j += maxY - 1) {
        infiniteIndexes.add(field[i][j])
      }
    }

    const sizes = coords
      .filter(x => !infiniteIndexes.has(x.index))
      .map(x => x.index)
      .map(x => field.flatMap(y => y).filter(y => y === x).length)

    return Math.max(...sizes)
  }

  part2 (input: string): number {
    const coords = this.prepareInput(input)
    const maxX = Math.max(...coords.map(x => x.x)) + 1
    const maxY = Math.max(...coords.map(x => x.y)) + 1

    const field: number[][] = []
    for (let i = 0; i < maxX; i++) {
      field[i] = []
      for (let j = 0; j < maxY; j++) {
        field[i][j] = -1
      }
    }

    coords.forEach(x => field[x.x - 1][x.y - 1] = x.index)
    this.fill2(field, coords)

    return field.flatMap(x => x).filter(x => x < this.searchDist!).length
  }

  private prepareInput (input: string): ICoord[] {
    return input.split('\n')
      .map(x => x.trim().split(', ').map(y => +y))
      .map((x, i) => ({
        x: x[0],
        y: x[1],
        index: i
      }))
  }

  private manhattanDistance (a: ICoord, b: ICoord): number {
    return Math.abs(a.x - b.x) + Math.abs(a.y - b.y)
  }

  private fill (field: number[][], coords: ICoord[]) {
    field.forEach((f, x) => {
      f.forEach((_, y) => {
        const dists = coords.map(z => this.manhattanDistance(z, { x, y, index: 0 }))
        const min = Math.min(...dists)
        if (dists.filter(z => z === min).length === 1) field[x][y] = dists.findIndex(z => z === min)
      })
    })
  }

  private fill2 (field: number[][], coords: ICoord[]) {
    field.forEach((f, x) => {
      f.forEach((_, y) => {
        field[x][y] = coords
          .map(z => this.manhattanDistance(z, { x, y, index: 0 }))
          .reduce((prev, curr) => prev + curr, 0)
      })
    })
  }
}

interface ICoord {
  x: number
  y: number
  index: number
}
