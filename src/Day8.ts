import { IDay } from './IDay'

export default class Day8 implements IDay<number> {
  part1 (input: string): number {
    const tree = this.parse(this.prepareInput(input))
    return tree.total
  }

  part2 (input: string): number {
    const tree = this.parse(this.prepareInput(input))
    return tree.value
  }

  private prepareInput (input: string): number[] {
    return input.trim().split(' ').map(x => +x)
  }

  private parse (input: number[]): Node {
    const copyInput = Array.from(input)
    const [child, meta] = copyInput.splice(0, 2)
    const root = new Node(child, meta)

    root.metadata = copyInput.splice(-meta, meta)
    root.parseChildren(copyInput)

    return root
  }
}

class Node {
  childCount: number
  metaCount: number
  metadata: number[] = []
  children: Node[] = []

  constructor (childCount: number, metaCount: number) {
    this.childCount = childCount
    this.metaCount = metaCount
  }

  get length (): number {
    const outerLength = this.metaCount + 2 // meta + header
    return this.childLength + outerLength
  }

  get childLength (): number {
    return this.children.reduce((prev, curr) => prev + curr.length, 0)
  }

  get total (): number {
    const nodeTotal = this.metadata.reduce((p, c) => p + c, 0)
    return this.children.reduce((prev, curr) => prev + curr.total, nodeTotal)
  }

  get value (): number {
    if (this.childCount === 0) {
      return this.metadata.reduce((p, c) => p + c, 0)
    } else {
      return this.metadata.map(x => this.children[x - 1])
        .filter(x => !!x)
        .reduce((prev, curr) => prev + curr.value, 0)
    }
  }

  parseChildren (data: number[], parsedCount: number = 0) {
    if (parsedCount === this.childCount) return
    const copyData = Array.from(data)

    const [child, meta] = copyData.splice(0, 2)
    const childNode = new Node(child, meta)
    if (child > 0) childNode.parseChildren(copyData)
    copyData.splice(0, childNode.childLength)
    childNode.metadata = copyData.splice(0, meta)

    this.children.push(childNode)
    if (copyData.length > 0) this.parseChildren(copyData, parsedCount + 1)
  }
}
