let implementation = `import { IDay } from './IDay'

export default class Day{day} implements IDay<number> {
  part1 (input: string): number {
    return +input
  }

  part2 (input: string): number {
    return +input
  }
}
`

let test = `import { SetupFixture, Test, TestFixture } from 'alsatian'
import Day{day} from '../src/Day{day}'
import { ITestBase } from '../src/IDay'

@TestFixture('Day {day} Tests')
export default class Day{day}Test implements ITestBase {
  runner?: Day{day}

  @SetupFixture
  setupFixture () {
    this.runner = new Day{day}()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    // TODO
  }

  @Test('Part 2 behaves')
  testPart2 () {
    // TODO
  }

  // tslint:disable-next-line:member-ordering max-line-length
  data: string = ''
}
`

if (process.argv.length < 3) {
  console.error('Must pass the day number to the script to continue')
} else {
  const fs = require('fs')
  const day = process.argv[2]
  fs.writeFileSync(`./src/Day${day}.ts`, implementation.replace(/{day}/g, day))
  fs.writeFileSync(`./test/Day${day}Test.ts`, test.replace(/{day}/g, day))
}
